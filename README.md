# Instructions

This Project is a simple polling system. Before you test it...

  - You need to install the node & npm.
  - You need to create a local server environment for MYSQL server.

Steps:
  1. git clone https://gitlab.com/5kahoisaac/scmp-home-assignment.git
  2. Using local server environment software like: MAMP
  3. Switch on the MAMP and go to its phpmyadmin panel
  4. Import the .sql file in the phpmyadmin panel
  5. Open the Terminal or Command line in your computer
  6. cd the backend folder and run php -S localhost:8000
  7. cd the frontend folder, run npm install and npm start

P.S.
You need to go to the './frontend/src/Config/index.js' to set your production version's api key before build the product.

> Isaac NG, Ka Ho (HK)
> Web & Hybird App Developer
> Contact: 5kahoisaac@gmail.com
