import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import 'antd/dist/antd.css';

import Router from './Router';
import reduxData from './Reducer/';
import Loading from './Component/Loading';
import * as serviceWorker from './serviceWorker';

const store = createStore(
  reduxData,
  applyMiddleware(
    thunkMiddleware,
    createLogger(),
  ),
);

const root = document.getElementById('root');

if (root) {
  ReactDOM.render(
    <Provider store={store}>
      <div>
        <Loading />
        <Router />
      </div>
    </Provider>
  , root);
  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: http://bit.ly/CRA-PWA
  serviceWorker.unregister();
}
