import React from 'react';
import { Doughnut, Chart } from 'react-chartjs-2';

const originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
  draw: function () {
    originalDoughnutDraw.apply(this, arguments);

    const chart = this.chart;
    const width = chart.chart.width;
    const height = chart.chart.height;
    const ctx = chart.chart.ctx;

    let fontSize = (height / 94).toFixed(2);
    ctx.font = "bold " + fontSize + "em sans-serif";
    ctx.fillStyle = "black";
    ctx.textBaseline = "middle";

    const text = '%';
    const textX = Math.round((width - ctx.measureText(text).width) / 2);
    const textY = height / 2;
    ctx.fillText(text, textX, textY);
  },
});

const options = {
  cutoutPercentage: 50,
  legend: {
    display: false,
    position: 'bottom',
    labels: {
      fontSize: 18,
    },
  },
  tooltips: {
    callbacks: {
      title: function(tooltipItem, data) {
        return data['labels'][tooltipItem[0]['index']];
      },
      label: function(tooltipItem, data) {
        const dataset = data['datasets'][0];
        const percent = Math.round((dataset['data'][tooltipItem['index']] / Object.values(dataset["_meta"])[0]['total']) * 100)
        return percent + '%';
      },
    },
  }
};


const Charts = ({ height, identity, chartData }) => {
  return (
    <Doughnut
      key={identity}
      height={height}
      options={options}
      data={
        {
          labels: chartData.title,
          datasets: [{
            label: chartData.title,
            data: chartData.value,
            borderWidth: 0,
            backgroundColor: chartData.color,
          }],
        }
      }
    />
  );
};


export default Charts;
