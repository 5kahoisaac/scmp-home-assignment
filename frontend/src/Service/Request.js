import axios from 'axios'
import { message } from 'antd';
import { ApiKey } from './../Config';

const service = axios.create({
  baseURL: ApiKey,
  timeout: 30000,
});

service.interceptors.request.use(
  config => {
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    config.crossDomain = true;
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.status !== 'success') {
      message.error(res.message);
      return Promise.reject('error');
    } else {
      if (res.message !== '') message.success(res.message);
      return response.data;
    }
  },
  error => {
    // const err = error.response);
    message.error('CAN NOT CONNECT TO SERVER');
    return Promise.reject(error)
  }
);

export default service;
