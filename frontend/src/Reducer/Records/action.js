import * as type from './../type';
import { sendRequest } from './../../Service/sendRequest';

export function getRecords() {
  return async (dispatch) => {
    let result = await sendRequest('?action=record');
    result = result.data;
    dispatch({
      type: type.GET_RECORDS,
      data: result,
    });
  }
}

export function updateRecords(q, a) {
  return async (dispatch) => {
    await sendRequest(`?action=vote&q=${q}&a=${a}`);
    dispatch({
      type: type.UPDATE_RECORDS,
      data: { question_id: q, answer_id: a },
    });
  }
}
