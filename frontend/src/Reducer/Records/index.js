import * as type from './../type';

export default function records(
  state = { total: {}, list: {} }, action,
) {
  switch (action.type) {
    case type.GET_RECORDS: {
      return action.data;
    }
    case type.UPDATE_RECORDS: {
      const newState = Object.assign({}, state);
      newState['total'][action.data.question_id] = Number(newState['total'][action.data.question_id]) + 1;
      newState['list'][action.data.answer_id] = Number(newState['list'][action.data.answer_id]) + 1;
      return newState;
    }
    default:
      return state;
  }
}
